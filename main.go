package main

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"gitlab.com/love-letter-ufaz-se/love-letter-game-api-app/v2/game_proto"
	"google.golang.org/grpc"
)

type clientsMux struct {
	clients map[*websocket.Conn]bool
	mux     sync.Mutex
}

var clients = clientsMux{
	clients: make(map[*websocket.Conn]bool),
}

var userSockets = make(map[int]*websocket.Conn)
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

var socketMsgs = make(chan string, 50)

var redisClient *redis.Client

func main() {

	redisClient = redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})
	defer redisClient.Close()

	_, err := redisClient.Ping().Result()
	handleErr("Can't ping redis server", err)

	router := mux.NewRouter()
	router.HandleFunc("/", wsHandler)
	go loopClients()
	go notifyPlayers()
	go parseCmd()

	log.Fatal(http.ListenAndServe(":8080", router))
}

func wsHandler(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	clients.mux.Lock()
	clients.clients[ws] = true
	clients.mux.Unlock()
}

func loopClients() {
	for {
		clients.mux.Lock()
		for key, flag := range clients.clients {
			if flag {
				go readMessages(key)
			}
		}
		clients.mux.Unlock()
		time.Sleep(100 * time.Millisecond)
	}
}

func readMessages(key *websocket.Conn) {
	for {
		clients.mux.Lock()
		clients.clients[key] = false
		clients.mux.Unlock()
		msgType, msg, err := key.ReadMessage()
		if err != nil {
			clients.mux.Lock()
			delete(clients.clients, key)
			clients.mux.Unlock()
			return
		}

		if msgType == websocket.TextMessage {
			log.Println("Websocket msg received:" + string(msg))
			cmdarr := strings.Split(string(msg), ":")
			if len(cmdarr) < 2 {
				clients.mux.Lock()
				clients.clients[key] = true
				clients.mux.Unlock()
				return
			}
			userID, err := strconv.Atoi(cmdarr[0])
			if err != nil {
				clients.mux.Lock()
				clients.clients[key] = true
				clients.mux.Unlock()
				return
			}

			if userSockets[userID] == nil {
				userSockets[userID] = key
			}
			socketMsgs <- string(msg)
		}
	}
}

func parseCmd() {
	opts := grpc.WithInsecure()
	gameGRPC, err := grpc.Dial("game-app:50051", opts)
	handleErr("Could not create connection between session-app", err)
	defer gameGRPC.Close()

	client := game_proto.NewGameAppClient(gameGRPC)
	for msg := range socketMsgs {
		cmdarr := strings.Split(string(msg), ":")
		if len(cmdarr) < 2 {
			continue
		}
		userID, err := strconv.Atoi(cmdarr[0])
		if err != nil {
			continue
		}

		switch cmdarr[1] {
		case "start_game":
			request := &game_proto.Player{
				Id: int64(userID),
			}
			client.NewGame(context.Background(), request)

		case "game_ready":
			// TODO: send new_turn after all players are ready
			userSockets[userID].WriteMessage(websocket.TextMessage, []byte("new_turn"))
		case "game_draw_card":
			gameID, err := strconv.Atoi(cmdarr[2])
			if err != nil {
				userSockets[userID].WriteMessage(websocket.TextMessage, []byte("wrong_op"))
			}
			request := &game_proto.GamePlayer{
				Gm: &game_proto.Game{
					Id: int64(gameID),
				},
				Plyr: &game_proto.Player{
					Id: int64(userID),
				},
			}
			resp, _ := client.DrawCard(context.Background(), request)
			if resp.Id != 0 {
				userSockets[userID].WriteMessage(websocket.TextMessage, []byte("turn:"+strconv.Itoa(userID)+":"+strconv.FormatInt(resp.Id, 10)))
			} else {
				winnerRequset := &game_proto.Game{
					Id: int64(gameID),
				}
				winner, _ := client.WhoWins(context.Background(), winnerRequset)
				notifyVictory(int64(gameID), winner.Id)
			}
		case "game_do_card":
			// msg = "<user_id>:game_do_card:<game_id>:<card_num>:<extra>"
			// resp = "end_of_turn:<user_ID>:<card_num>:<additional_str>"
			gameID, err := strconv.Atoi(cmdarr[2])
			if err != nil {
				userSockets[userID].WriteMessage(websocket.TextMessage, []byte("wrong_op"))
			}
			cardID, err := strconv.Atoi(cmdarr[3])
			if err != nil {
				userSockets[userID].WriteMessage(websocket.TextMessage, []byte("wrong_op"))
			}

			request := &game_proto.GamePlayerCard{
				Gm: &game_proto.Game{
					Id: int64(gameID),
				},
				Plyr: &game_proto.Player{
					Id: int64(userID),
				},
				Crd: &game_proto.Card{
					Id: int64(cardID),
				},
			}

			client.DiscardCard(context.Background(), request)

			userIDs, err := redisClient.ZRange("game:"+strconv.Itoa(gameID)+":users", 0, 5).Result()

			for _, user := range userIDs {
				if user != strconv.Itoa(userID) {
					userInt, err := strconv.Atoi(user)
					if err != nil {
						continue
					}
					userSockets[userInt].WriteMessage(websocket.TextMessage, []byte("end_of_turn:"+strconv.Itoa(userID)+":"+strconv.Itoa(cardID)+":"+strings.Join(cmdarr[4:], ":")))
				}
				userInt, err := strconv.Atoi(user)
				if err != nil {
					continue
				}
				userSockets[userInt].WriteMessage(websocket.TextMessage, []byte("new_turn"))
			}

		}
	}
}

func notifyPlayers() {
	opts := grpc.WithInsecure()
	gameGRPC, err := grpc.Dial("game-app:50051", opts)
	handleErr("Could not create connection between session-app", err)
	defer gameGRPC.Close()

	client := game_proto.NewGameAppClient(gameGRPC)
	for {
		res, err := redisClient.LLen("games:notify").Result()
		if err != nil || res < 1 {
			time.Sleep(100 * time.Millisecond)
			continue
		}
		var i int64
		for i = 0; i < res; i++ {
			notifyGameID, err := redisClient.LPop("games:notify").Result()
			if err != nil {
				continue
			}

			gameIDInt, err := strconv.Atoi(notifyGameID)

			if err != nil {
				continue
			}
			request := &game_proto.Game{
				Id: int64(gameIDInt),
			}
			client.StartGame(context.Background(), request)

			userIDs, err := redisClient.ZRange("game:"+notifyGameID+":users", 0, 5).Result()

			for _, userID := range userIDs {
				userIDint, err := strconv.Atoi(userID)
				if err != nil {
					continue
				}
				if userSockets[userIDint] == nil {
					continue
				}
				userCard, err := redisClient.Get("game:" + notifyGameID + ":user:" + userID + ":card").Result()
				userSockets[userIDint].WriteMessage(websocket.TextMessage, []byte("game_start:"+userCard+":"+notifyGameID+":"+strings.Join(userIDs, ":")))
			}
		}
	}
}

func notifyVictory(gameID int64, winnerID int64) {
	userIDs, _ := redisClient.ZRange("game:"+strconv.FormatInt(gameID, 10)+":users", 0, 5).Result()

	for _, userID := range userIDs {
		userIDint, err := strconv.Atoi(userID)
		if err != nil {
			continue
		}
		if userSockets[userIDint] == nil {
			continue
		}
		userSockets[userIDint].WriteMessage(websocket.TextMessage, []byte("victory:"+strconv.FormatInt(winnerID, 10)))
	}
}

func handleErr(msg string, err error) {
	if err != nil {
		log.Fatalf("%s ... %v\n", msg, err)
	}
}

/*

switch(cmd){
            case "game_start":
				return Command.GAME_START
				// msg = "game_start:<card_id>:<game_id>:...<user_IDs>"
            case "new_turn":
				return Command.NEW_TURN
            case "turn":
                return Command.TURN
				// msg = "turn:<user_id>:<card_num>"
            case "end_of_turn":
				return Command.END_OF_TURN
				// msg = "end_of_turn:<user_ID>:<card_num>:<additional_str>"
            default:
                return Command.UNKNOWN_CMD
		}

*/
