module gitlab.com/love-letter-ufaz-se/love-letter-game-api-app/v2

go 1.14

require (
	github.com/go-redis/redis/v7 v7.2.0
	github.com/golang/protobuf v1.3.3
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	google.golang.org/grpc v1.29.1
)
